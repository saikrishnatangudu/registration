package com.hcl.Exception;

public class CourseNotFoundException extends RuntimeException{
	public CourseNotFoundException () {
		super(String.format("course with  Id  not found "));
	}

}
