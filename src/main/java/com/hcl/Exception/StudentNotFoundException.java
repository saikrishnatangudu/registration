package com.hcl.Exception;

public class StudentNotFoundException extends RuntimeException{
	public StudentNotFoundException () {
		super(String.format("Student with  Id  not found "));
	}

}
