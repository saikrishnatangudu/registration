package com.hcl.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class Course implements Serializable {
	
	private int courseId;

	private String courseName;
	
	
	private Set<Student> students = new HashSet<Student>();

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public int getCourseId() {
		return courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	//@ManyToMany(fetch = FetchType.LAZY, mappedBy = "courses")
	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Course(int courseId, String courseName, Set<Student> students) {
		super();
		this.courseId = courseId;
		this.courseName = courseName;
		this.students = students;
	}

	public Course() {
		super();
		// TODO Auto-generated constructor stub
	}

}
