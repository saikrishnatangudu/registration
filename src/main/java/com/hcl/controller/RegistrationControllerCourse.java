package com.hcl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.model.Course;
import com.hcl.service.RegistrationService;

@RestController
@RequestMapping("/registration")
public class RegistrationControllerCourse {
	@Autowired
	RegistrationService registrationService;
	
	@GetMapping("/courses")
	public ResponseEntity<String> getAllCourses() {
		String course= registrationService.getCourses();
		return new ResponseEntity<>(course,HttpStatus.OK);
	}
	
	@GetMapping("course/{courseId}")
	public ResponseEntity<String> getByStudentId(@PathVariable String courseId){
		String course= registrationService.getByCourseId(courseId);
		return new ResponseEntity<>(course,HttpStatus.OK);
	}
	@PostMapping("course/")
	public ResponseEntity<String> createstudent(@RequestBody Course course){
		
		return registrationService.createCourse(course);
		
			
	}
	
	@GetMapping("course1")
	public ResponseEntity<String> getStudentIdByParam(@RequestParam String courseId){
		String course= registrationService.getCourseByParam(courseId);
		 return new ResponseEntity<>(course,HttpStatus.OK);
	}
	
}
