package com.hcl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.hcl.model.Student;
import com.hcl.service.RegistrationService;

@RestController
@RequestMapping("/registration")
public class RegistrationControllerStudent {

	@Autowired
	RegistrationService registrationService;
	
	
	@GetMapping("/students")
	public ResponseEntity<String> getAllStudents() {
		String student= registrationService.getUserStudents();
		return new ResponseEntity<>(student,HttpStatus.OK);
	}
	
	@GetMapping("student/{userId}")
	public ResponseEntity<String> getByStudentId(@PathVariable String userId){
		String student= registrationService.getByStudentId(userId);
		return new ResponseEntity<>(student,HttpStatus.OK);
	}
	@PostMapping("student/")
	public ResponseEntity<String> createstudent(@RequestBody Student student){
		
		return registrationService.createStudent(student);
		
			
	}
	
	@GetMapping("student1")
	public ResponseEntity<String> getStudentIdByParam(@RequestParam String userId){
		String student= registrationService.getUserBypram(userId);
		 return new ResponseEntity<>(student,HttpStatus.OK);
	}
	
	
}
