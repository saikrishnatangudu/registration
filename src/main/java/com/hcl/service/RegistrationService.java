package com.hcl.service;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.hcl.Exception.CourseNotFoundException;
import com.hcl.Exception.DataNotFoundException;
import com.hcl.Exception.StudentNotFoundException;
import com.hcl.model.Course;
import com.hcl.model.Student;

@Service
public class RegistrationService {
	@Autowired
	RestTemplate restTemplate;

	@Bean
	@LoadBalanced
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	public String getUserStudents() {
		try {
			String result = restTemplate.getForObject("http://STUDENT-SERVICE/students", String.class);
			return result;
		} catch (Exception e) {
			throw new DataNotFoundException();
		}
	}

	public String getByStudentId(String userId) {

		String url = "http://STUDENT-SERVICE/student/" + userId;
		try {
			String result = restTemplate.getForObject(url, String.class);
			return result;
		} catch (Exception e) {
			throw new StudentNotFoundException();
		}
	}

	public ResponseEntity<String> createStudent(Student student) {
		String uri = "http://STUDENT-SERVICE/student";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("studentId", student.getStudentId());
		request.put("studentName", student.getStudentName());
		request.put("courses", student.getCourses());

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

		System.out.println(response);
		return response;
	}

	public String getUserBypram(String userId) {

		String url = "http://STUDENT-SERVICE/student/" + userId;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		Map<String, String> params = new HashMap<String, String>();
		params.put("userId", userId);

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		for (Map.Entry<String, String> entry : params.entrySet()) {
			builder.queryParam(entry.getKey(), entry.getValue());
		}

		String result = restTemplate.getForObject(builder.toUriString(), String.class);
		return result;

	}

	public String getCourses() {
		try {
			String result = restTemplate.getForObject("http://COURSE-SERVICE/courses", String.class);
			return result;
		} catch (Exception e) {
			throw new DataNotFoundException();
		}
	}

	public String getByCourseId(String courseId) {

		String url = "http://COURSE-SERVICE/course/" + courseId;
		try {
			String result = restTemplate.getForObject(url, String.class);
			return result;
		} catch (Exception e) {
			throw new CourseNotFoundException();
		}
	}

	public ResponseEntity<String> createCourse(Course course) {

		String uri = "http://COURSE-SERVICE/course";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("courseId", course.getCourseId());
		request.put("courseName", course.getCourseName());
		request.put("students", course.getStudents());

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

		System.out.println(response);
		return response;
	}

	public String getCourseByParam(String courseId) {

		String url = "http://COURSE-SERVICE/course/" + courseId;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		Map<String, String> params = new HashMap<String, String>();
		params.put("courseId", courseId);

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		for (Map.Entry<String, String> entry : params.entrySet()) {
			builder.queryParam(entry.getKey(), entry.getValue());
		}

		String result = restTemplate.getForObject(builder.toUriString(), String.class);
		return result;

	}

}
